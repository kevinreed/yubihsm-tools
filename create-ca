#!/bin/bash

savelogs() {
  LOG=$(mktemp /tmp/ca/${1}.log.XXXXXX)
  yubihsm-shell --authkey 1 --password password --action get-logs --out "${LOG}"
  sleep 1
}

./prepare-environment

## HSM setup
echo -ne '\007'
echo "Setting up keys. Insert 1st YubiHSM module."
read -p "Press enter to continue"
# reset the HSM
yubihsm-shell --authkey 1 --password password --action reset
sleep 5

savelogs hsm-1

rm -f /tmp/ca/*

# generate asymmetric keys
yubihsm-shell --authkey 1 --password password --action generate-asymmetric-key --object-id 1 --label "Root EC key"  --domains 1 --capabilities exportable-under-wrap:sign-ecdsa --algorithm ecp384
sleep 1
yubihsm-shell --authkey 1 --password password --action generate-asymmetric-key --object-id 2 --label "Root RSA key" --domains 1 --capabilities exportable-under-wrap,sign-pkcs,sign-pss  --algorithm rsa3072
sleep 1
savelogs hsm-1

# generate wrap key and import it in the 1st YubiHSM
yubihsm-shell --authkey 1 --password password -a get-pseudo-random  --count=32 --out=/tmp/ca/wrap.key
sleep 1
yubihsm-shell --authkey 1 --password password --action put-wrap-key --object-id 3 --label "Export key" --domains 1 --capabilities export-wrapped --delegated exportable-under-wrap,sign-ecdsa,sign-pkcs,sign-pss --in=/tmp/ca/wrap.key
sleep 1

# export asymmetric keys under wrap
yubihsm-shell --authkey 1 --password password --action get-wrapped --object-id 3 --object-type asymmetric-key --wrap-id 1 --out=/tmp/ca/key1.wrapped
sleep 1
yubihsm-shell --authkey 1 --password password --action get-wrapped --object-id 3 --object-type asymmetric-key --wrap-id 2 --out=/tmp/ca/key2.wrapped
sleep 1

# then delete wrap key
yubihsm-shell --authkey 1 --password password --action delete-object --object-id 3 --object-type wrap-key
savelogs hsm-1

# create needed users on the 1st YubiHSM
export YUBIHSM_AUTH_KEY="0x1"
export YUBIHSM_AUTH_PW="password"
./yubihsm-adduser admin 0xa
./yubihsm-adduser admin 0xb
./yubihsm-adduser operator 0xc
./yubihsm-adduser operator 0xd
./yubihsm-adduser auditor 0xe
./yubihsm-adduser auditor 0xf

# Retrieve the final HSM log batch and clean up internal log
export LOG=$(mktemp /tmp/ca/hsm-1.log.XXXXXX)
yubihsm-shell --authkey 1 --password password --action get-logs --out "${LOG}"
sleep 1
LOGIDX=$(tail -1 "${LOG}" | awk '{print $2}')
unset LOG
yubihsm-shell --authkey 1 --password password --action set-log-index --log-index "${LOGIDX}"
sleep 1

# delete superuser
yubihsm-shell --authkey 1 --password password --action delete-object --object-id 1 --object-type authentication-key

# import wrap key into the 2nd YubiHSM
# note that the key has different capability
echo -ne '\007'
read -p "Insert 2nd YubiHSM and press enter to continue"
yubihsm-shell --authkey 1 --password password --action reset
sleep 5
savelogs hsm-2

yubihsm-shell --authkey 1 --password password --action put-wrap-key --object-id 3 --label "Import key" --domains 1 --capabilities import-wrapped --delegated exportable-under-wrap,sign-ecdsa,sign-pkcs,sign-pss --in=/tmp/ca/wrap.key
sleep 1
yubihsm-shell --authkey 1 --password password --action put-wrapped  --wrap-id 3 --in /tmp/ca/key1.wrapped
sleep 1
yubihsm-shell --authkey 1 --password password --action put-wrapped  --wrap-id 3 --in /tmp/ca/key2.wrapped

# then delete wrap key
yubihsm-shell --authkey 1 --password password --action delete-object --object-id 3 --object-type wrap-key
savelogs hsm-2

# clean up exported data
shred --remove /tmp/ca/key1.wrapped /tmp/ca/key2.wrapped /tmp/ca/wrap.key

# create needed users on the 2nd YubiHSM
./yubihsm-adduser admin 0xa
./yubihsm-adduser admin 0xb
./yubihsm-adduser operator 0xc
./yubihsm-adduser operator 0xd
./yubihsm-adduser auditor 0xe
./yubihsm-adduser auditor 0xf

# Retrieve the final HSM log batch and clean up internal log
export LOG=$(mktemp /tmp/ca/hsm-2.log.XXXXXX)
yubihsm-shell --authkey 1 --password password --action get-logs --out "${LOG}"
sleep 1
LOGIDX=$(tail -1 "${LOG}" | awk '{print $2}')
unset LOG
yubihsm-shell --authkey 1 --password password --action set-log-index --log-index "${LOGIDX}"
sleep 1


# delete superuser
yubihsm-shell --authkey 1 --password password --action delete-object --object-id 1 --object-type authentication-key


## Log collection and cleanup
grep -h '^item:' /tmp/ca/hsm-1.log.*|sort -k2g -u > /home/user/ca/hsm-1.log
grep -h '^item:' /tmp/ca/hsm-2.log.*|sort -k2g -u > /home/user/ca/hsm-2.log
