# Tools to simplify YubiHSM management

YubiHSM is an inexpensive HSM with USB interface. Tools that help using it:

- `yubihsm-adduser` - creates user with pre-defined role
